from flask import Flask, render_template,request, redirect, url_for, flash
from flask_mysqldb import MySQL
import smtplib 




app = Flask(__name__)
app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'complaints'

mysql = MySQL(app)

@app.route('/') 
def home():
    
    return render_template('home.html')

@app.route('/insert', methods = ['POST'])
def insert():

    if request.method == "POST":
        flash(" Successfully")
        name = request.form['name']
        contact = request.form['contact']
        email = request.form['email']
        doorno = request.form['doorno']
        address = request.form['address']
        city = request.form['city']
        state = request.form['state']
        country = request.form['country']
        postal_code = request.form['postal_code']
        police_s = request.form['police_s']
        complaint = request.form['complaint']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO peopleinfo (name, contact, email, doorno, address, city, state, country, postal_code, police_s, complaint) VALUES (%s, %s, %s,%s, %s, %s,%s, %s, %s,%s, %s)", (name, contact, email, doorno, address, city, state, country, postal_code, police_s, complaint))
        mysql.connection.commit()
        message="Thank You For Visiting Online FIR, Your Complaint Successfully Send To The Police Station, Do Not Reply This Message."
        server=smtplib.SMTP("smtp.gmail.com",587)
        server.starttls()
        server.login("firregister005@gmail.com","num@0909")
        server.sendmail("firregister005@gmail.com", email,message)
        
        
        
        return render_template('message.html',name=name)
       # return redirect(url_for('home'))
@app.route('/admin',methods = ['GET','POST']) 
def admin():
    cur = mysql.connection.cursor()
    cur.execute("SELECT  * FROM peopleinfo")
    data = cur.fetchall()
    cur.close()

    return render_template('admin.html',firs=data)

@app.route('/delete/<string:id_data>', methods = ['GET','POST'])
def delete(id_data):
    flash("Record Has Been Deleted Successfully")
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM peopleinfo WHERE id=%s", [id_data])
    mm=cur.fetchall()
    
    for data in mm:
        dd=data[3]
    
    
    cur.execute("DELETE FROM peopleinfo WHERE id=%s", [id_data])
    mysql.connection.commit()
    message="Thank You For Visiting Online FIR,But Your Complaint Is Rejected By Police Station Authority. Please Try Again, Do Not Reply This Message."
    server=smtplib.SMTP("smtp.gmail.com",587)
    server.starttls()
    server.login("firregister005@gmail.com","num@0909")
    server.sendmail("firregister005@gmail.com",dd,message)
    
    return redirect(url_for('admin'))

@app.route('/accept/<string:id_data>', methods = ['GET','POST'])
def accept(id_data):
    flash("Record Has Been Accept Successfully")
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM peopleinfo WHERE id=%s", [id_data])
    mm=cur.fetchall()
    
    for data in mm:
        dd=data[3]
    
    
    
    mysql.connection.commit()
    message="Thank You For Visiting Online FIR, Your Complaint Is Accept By Police Station Authority. Do Not Reply This Message."
    server=smtplib.SMTP("smtp.gmail.com",587)
    server.starttls()
    server.login("firregister005@gmail.com","num@0909")
    server.sendmail("firregister005@gmail.com",dd,message)
    
    return redirect(url_for('admin'))

@app.route('/login', methods = ['GET','POST']) 
def login():
    
    email="chemate@gmail.com"
    password="123456"
    if request.method=="POST":
        mail = request.form['email']
        adminpass = request.form['password']
        
        if email==str(mail) and password ==str(adminpass):
            
            flash("Login Successfully")
            return redirect(url_for('admin'))
        else:       
            flash("Login Unsuccessfully")    
            return redirect(url_for('home'))        
        
    
   
            
if __name__ == "__main__":
    app.run(debug=True)