-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 04:26 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `complaints`
--

-- --------------------------------------------------------

--
-- Table structure for table `peopleinfo`
--

CREATE TABLE `peopleinfo` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact` int(250) NOT NULL,
  `email` varchar(100) NOT NULL,
  `doorno` int(11) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `postal_code` int(230) NOT NULL,
  `police_s` varchar(100) NOT NULL,
  `complaint` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peopleinfo`
--

INSERT INTO `peopleinfo` (`id`, `name`, `contact`, `email`, `doorno`, `address`, `city`, `state`, `country`, `postal_code`, `police_s`, `complaint`) VALUES
(43, 'mayur', 987654321, 'mayurghadage40@gmail.com', 0, '', '', '', '', 0, 'Narayangoan', 'helloo'),
(45, 'dipak', 2147483647, 'gadhavedeepak29@gmail.com', 0, '', '', '', '', 0, 'Alephata', 'hello'),
(46, 'dipak', 2147483647, 'gadhavedeepak29@gmail.com', 0, '', '', '', '', 0, 'Alephata', 'hello'),
(47, 'dips', 987654322, 'gadhavedeepak29@gmail.com', 0, '', '', '', '', 0, 'Narayangoan', 'sss'),
(49, 's', 1234587654, 'gadhavedeepak29@gmail.com', 0, '', '', '', '', 0, 'Narayangoan', 'hello'),
(50, 's', 1234587654, 'gadhavedeepak29@gmail.com', 0, '', '', '', '', 0, 'Narayangoan', 'hello'),
(51, 'dd', 2147483647, 'firregister005@gmail.com', 0, '', '', '', '', 0, 'Belhe', 'hoi'),
(52, 'dd', 2147483647, 'firregister005@gmail.com', 0, '', '', '', '', 0, 'Belhe', 'hoi'),
(53, 'dd', 2147483647, 'firregister005@gmail.com', 0, '', '', '', '', 0, 'Belhe', 'hoi'),
(54, 'dd', 2147483647, 'firregister005@gmail.com', 0, '', '', '', '', 0, 'Belhe', 'hoi'),
(57, 'satyawan', 2147483647, 'satyawan050@gmail.com', 1, 'manjarwadi', 'pune', 'MH', 'India', 410504, 'Narayangoan', 'testing complaint'),
(58, 'pratiksha', 1234567890, 'pratikshagadhave141@gmail.com', 1900, 'shingave Par', 'Manchar', 'Maharashtra', 'India', 234526, 'Manchar', 'kfgdkhfus'),
(59, 'dipak gopala ghADAGE ', 2147483647, 'dipakghadage7028@gmail.com', 0, 'AP PIMPRI PENDHAR TAAL JUNNER  DIST PUNE PUNE', 'PUNE', 'MAHARASHTRA', 'INDIA', 410504, 'Alephata', 'MALA SHEJARCHI TRASS DETATA..'),
(60, 'dipak gopala ghADAGE ', 2147483647, 'dipakghadage7028@gmail.com', 0, 'AP PIMPRI PENDHAR TAAL JUNNER  DIST PUNE PUNE', 'PUNE', 'MAHARASHTRA', 'INDIA', 410504, 'Alephata', 'MALA SHEJARCHI TRASS DETATA..');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `peopleinfo`
--
ALTER TABLE `peopleinfo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `peopleinfo`
--
ALTER TABLE `peopleinfo`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
